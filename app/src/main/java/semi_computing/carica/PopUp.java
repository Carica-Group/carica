package semi_computing.carica;

import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.Intent;
import android.content.IntentFilter;
import android.content.SharedPreferences;
import android.os.Build;
import android.os.Bundle;
import android.support.v4.hardware.fingerprint.FingerprintManagerCompat;
import android.support.v7.app.AppCompatActivity;
import android.util.DisplayMetrics;
import android.view.KeyEvent;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.TextView;
import android.widget.Toast;

import com.android.volley.Request;
import com.android.volley.Response;
import com.android.volley.VolleyError;
import com.android.volley.toolbox.StringRequest;

import org.json.JSONException;
import org.json.JSONObject;

import java.util.HashMap;
import java.util.Map;


public class PopUp extends AppCompatActivity {

    private Button check, close;

    //for individualy Layout
    private TextView inputHelperMessage;
    private EditText passwordInput;
    private Button usePassword;
    private ImageView fingerabdruck;

    //Fingerprint
    private Boolean BsafeFinger;
    private FingerprintPay fingerprintPay;
    private FingerprintManagerCompat fingerprintManager;
    private SharedPreferences sharedPreferencesFinger;


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_popup);

        //Find Buttons and TextViews
        check = findViewById(R.id.check);
        close = findViewById(R.id.close);
        inputHelperMessage = findViewById(R.id.Popup);
        passwordInput = findViewById(R.id.PWcontrol);
        usePassword = findViewById(R.id.usePassword);
        fingerabdruck = findViewById(R.id.fingerabdruck);


        fingerabdruck.setImageResource(R.mipmap.ic_fingerprint_round);

        //Initialize Fingerprint Classes and Values
        fingerprintPay = new FingerprintPay(this);
        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.M){
            fingerprintManager = FingerprintManagerCompat.from(getApplicationContext());//(FingerprintManagerCompat) getSystemService(FINGERPRINT_SERVICE);
        }
        sharedPreferencesFinger = getSharedPreferences("Fingerref10", MODE_PRIVATE);
        BsafeFinger = sharedPreferencesFinger.getBoolean("BsafeFinger",false);

        //Contexts for PopUpFingerprint
        fingerprintPay.SetContext(getApplicationContext());
        fingerprintPay.SetActivity(this); //übergibt PopUp als Activity zum beenden

        IntentFilter fp_filter = new IntentFilter("com.carica.broadcast.FINGERPRINT_FAILED");
        this.registerReceiver(bR_FingerprintFailed, fp_filter);

        //Größe des PopUp-Feldes definieren
        DisplayMetrics dm = new DisplayMetrics();
        getWindowManager().getDefaultDisplay().getMetrics(dm);

        int width = dm.widthPixels;
        int height = dm.heightPixels;

        getWindow().setLayout((int) (width * 0.80), (int) (height * 0.5));

        //Closes the Activity with the Button 'close'
        close.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                finish();
            }
        });

        //Check the password by pressing Enter
        SetEnterKey();

        Authenticate();

    }

    private void SetEnterKey(){
        //check the password by pressing Enter
        passwordInput.setOnKeyListener(new View.OnKeyListener() {
            @Override
            public boolean onKey(View view, int keyCode, KeyEvent keyEvent) {
                if (keyEvent.getAction() == KeyEvent.ACTION_DOWN) {
                    switch (keyCode) {
                        case KeyEvent.KEYCODE_DPAD_CENTER:
                        case KeyEvent.KEYCODE_ENTER:
                            checkPw();
                            return true;
                        default:
                            break;
                    }
                }
                return false;
            }
        });
    }

    //Ein- und Ausblenden der gewünschten XML Elemente bei Fingerprint
    private boolean SetPopUpFingerprint()
    {
        inputHelperMessage.setText(R.string.input_helper_message_fingerprint);
        check.setVisibility(View.GONE);
        passwordInput.setVisibility(View.GONE);
        usePassword.setVisibility(View.VISIBLE);
        fingerabdruck.setVisibility(View.VISIBLE);
        //GONE ist ausgeblendet VISIBLE ist eingeblendet
        return true;
    }

    //Ein-und Ausblenden der gewünschten XML Elemente bei Passworteingabe
    private boolean SetPopUpPassword()
    {
        inputHelperMessage.setText(R.string.popup);
        check.setVisibility(View.VISIBLE);
        passwordInput.setVisibility(View.VISIBLE);
        usePassword.setVisibility(View.GONE);
        fingerabdruck.setVisibility(View.GONE);

        return true;
    }

    private void Authenticate(){

        if (BsafeFinger) {

            //for waiting until the layoutconfig is completed
            if(SetPopUpFingerprint()) {
                //Start Nice Fingerprint PopUp here
                fingerprintPay.startAuthentication(fingerprintManager, null);

                usePassword.setOnClickListener(new View.OnClickListener() {
                    @Override
                    public void onClick(View view) {
                        BsafeFinger = false;
                        fingerprintPay.stopListening();
                        Authenticate();
                    }
                });
            }
        }
        if (!BsafeFinger) {

            //waiting until the layoutconfig is completed
            if (SetPopUpPassword()) {
                check.setOnClickListener(new View.OnClickListener() {
                    @Override
                    public void onClick(View view) {
                        checkPw();
                    }
                });
            }
        }
    }

    public void checkPw()
    {
        final String checkPw = passwordInput.getText().toString();

        StringRequest request = new StringRequest(Request.Method.POST, Constants.PASSWORD_URL, new Response.Listener<String>()
        {
            @Override
            public void onResponse(String response)
            {
                try {
                    JSONObject json = new JSONObject(response);
                    if(!json.getBoolean("error"))
                    {
                        //password correct
                        Intent intent = new Intent();
                        intent.setAction("com.carica.broadcast.PW_CHECK_COMPLETED");
                        getApplicationContext().sendBroadcast(intent);
                        finish();
                    }
                    else{
                        String error = json.getString("message");
                        Toast.makeText(getApplicationContext(), error, Toast.LENGTH_LONG).show();
                    }
                }catch(JSONException e){
                    Toast.makeText(getApplicationContext(), e.toString(), Toast.LENGTH_LONG).show();
                }
            }
        }, new Response.ErrorListener()
        {
            @Override
            public void onErrorResponse(VolleyError error)
            {
                Toast.makeText(getApplicationContext(), error.toString(), Toast.LENGTH_LONG).show();
            }
        }) {
            @Override
            protected Map<String, String> getParams() {
                Map<String, String> params = new HashMap<>();
                //The Post parameters
                params.put("username", (SharedPrefManager.getInstance(getApplicationContext()).getUsername()));
                params.put("password", checkPw);

                return params;
            }
        };

        VolleySingleton.getInstance(this).addToRequestQueue(request);
    }

    private BroadcastReceiver bR_FingerprintFailed = new BroadcastReceiver() {
        @Override
        public void onReceive(Context context, Intent intent) {
            fingerabdruck.setImageResource(R.mipmap.ic_fingerprint_failed_round);
        }
    };
}