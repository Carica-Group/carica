package semi_computing.carica;

import android.app.KeyguardManager;
import android.app.ProgressDialog;
import android.content.Intent;
import android.content.SharedPreferences;
import android.os.Build;
import android.os.Bundle;
import android.support.v4.hardware.fingerprint.FingerprintManagerCompat;
import android.support.v7.app.AppCompatActivity;
import android.view.KeyEvent;
import android.view.View;
import android.view.inputmethod.EditorInfo;
import android.widget.Button;
import android.widget.CheckBox;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.RelativeLayout;
import android.widget.TextView;
import android.widget.Toast;

import com.android.volley.Request;
import com.android.volley.Response;
import com.android.volley.VolleyError;
import com.android.volley.toolbox.StringRequest;

import org.json.JSONException;
import org.json.JSONObject;

import java.util.HashMap;
import java.util.Map;

public class LoginActivity extends AppCompatActivity{

    SharedPreferences sharedPreferencesUsername;
    SharedPreferences sharedPreferencesStayLogged;
    SharedPreferences.Editor editorUsernname;
    SharedPreferences.Editor editorstayLogged;
    Boolean Bsafeusername;
    Boolean BstayLogged;
    CheckBox safeUsernamecheckbox;
    CheckBox safeStayLoggedcheckbox;
    private EditText editTextUsername, editTextPassword;
    private Button buttonLogin;
    private ProgressDialog progressDialog;
    SharedPreferences sharedPreferencesFinger;
    boolean BsafeFinger;
    FingerprintManagerCompat fingerprintManager;
    KeyguardManager keyguardManager;
    FingerprintLogin fingerprintLogin;
    private ImageView fingerprintIcon;
    private TextView usernamesaveTextView;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_login);

        editTextUsername = findViewById(R.id.Nutzername);
        editTextPassword = findViewById(R.id.Passwort);
        buttonLogin = findViewById(R.id.login);
        sharedPreferencesUsername = getSharedPreferences("loginref", MODE_PRIVATE);
        sharedPreferencesStayLogged = getSharedPreferences("loginref1", MODE_PRIVATE);
        safeUsernamecheckbox = findViewById(R.id.safeUsername);
        safeStayLoggedcheckbox = findViewById(R.id.stayLogged);
        Bsafeusername = sharedPreferencesUsername.getBoolean("Bsafeusername",false);
        BstayLogged = sharedPreferencesStayLogged.getBoolean("BstayLogged",false);
        sharedPreferencesFinger = getSharedPreferences("Fingerref10", MODE_PRIVATE);
        BsafeFinger = sharedPreferencesFinger.getBoolean("BsafeFinger",false);

        fingerprintIcon = findViewById(R.id.fingerprint_icon);
        usernamesaveTextView = findViewById(R.id.Nutzernamemerken);

        passwordLogin();
        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.M) {
            keyguardManager = (KeyguardManager) getSystemService(KEYGUARD_SERVICE);
            fingerprintManager = FingerprintManagerCompat.from(getApplicationContext());
            fingerprintLogin = new FingerprintLogin(this);
        }


        Check_Checkbox();

        //checks the Password by using the Passwordlayout
        if(!BsafeFinger)
        {
           checkViaPassword();
        }

        //checks the Fingerprint or the Password in case the used Layout
        if(BsafeFinger)
        {
            if(fingerprintLogin())
            {
                buttonLogin.setOnClickListener(new View.OnClickListener(){
                    @Override
                    public void onClick(View view) {
                        BsafeFinger = false;
                        fingerprintLogin.stopListening();
                        checkViaPassword();
                    }
                });
            }
        }

        safeUsernamecheckbox.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Safe_Checkbox_username();
            }
        });

        safeStayLoggedcheckbox.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Safe_Checkbox_StayLogged();
            }
        });

        editTextPassword.setOnEditorActionListener(new TextView.OnEditorActionListener() {
            public boolean onEditorAction(TextView v, int actionId, KeyEvent event) {
                if ((event != null && (event.getKeyCode() == KeyEvent.KEYCODE_ENTER)) || (actionId == EditorInfo.IME_ACTION_DONE)) {
                    progressDialog.show();
                    userLogin();
                }
                return false;
            }
        });
    }

    private boolean fingerprintLogin()
    {
        //Settings for the FingerprintLogin for better Layout
        buttonLogin.setText(R.string.usePW_button);
        editTextPassword.setVisibility(View.GONE);
        fingerprintIcon.setVisibility(View.VISIBLE);
        setBelow(R.id.fingerprint_icon);

        return true;
    }

    private boolean passwordLogin()
    {
        //Settings for the PasswordLogin Layout
        buttonLogin.setText(R.string.login);
        editTextPassword.setVisibility(View.VISIBLE);
        fingerprintIcon.setVisibility(View.GONE);
        setBelow(R.id.Passwort);

        return true;
    }

    //set anything after the Username input under the whised Element
    private void setBelow(int id)
    {
        RelativeLayout.LayoutParams params = (RelativeLayout.LayoutParams)safeUsernamecheckbox.getLayoutParams();
        params.removeRule(RelativeLayout.BELOW);
        params.addRule(RelativeLayout.BELOW, id);
        safeUsernamecheckbox.setLayoutParams(params);

        RelativeLayout.LayoutParams textview = (RelativeLayout.LayoutParams) usernamesaveTextView.getLayoutParams();
        textview.removeRule(RelativeLayout.BELOW);
        textview.addRule(RelativeLayout.BELOW, id);
        usernamesaveTextView.setLayoutParams(textview);
    }

    //Funktion checks if the Password is correct, is also nessesary in the FingerprintLayout
    private void checkViaPassword()
    {
        if(passwordLogin()) {
            buttonLogin.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    userLogin();
                }
            });
        }
    }

    private void Check_Checkbox(){
        //Checkbox Abfragen
        if(BstayLogged && Bsafeusername){
            if(SharedPrefManager.getInstance(this).isLoggedIn()){
                editTextUsername.setText(sharedPreferencesUsername.getString("username", null));
                safeUsernamecheckbox.setChecked(true);
                safeStayLoggedcheckbox.setChecked(true);
                finish();
                startActivity(new Intent(this, MainActivity.class));
                return;
            }
            else{
                if (BsafeFinger) {
                    editTextUsername.setText(sharedPreferencesUsername.getString("username", null));
                    safeUsernamecheckbox.setChecked(true);
                    safeStayLoggedcheckbox.setChecked(true);
                    fingerprintLogin.startAuthentication(fingerprintManager, null);
                }
                else{
                    editTextUsername.setText(sharedPreferencesUsername.getString("username", null));
                    safeUsernamecheckbox.setChecked(true);
                    safeStayLoggedcheckbox.setChecked(true);
                }
            }
        }
        if(BstayLogged && !Bsafeusername){
            if(SharedPrefManager.getInstance(this).isLoggedIn()){
                safeUsernamecheckbox.setChecked(false);
                safeStayLoggedcheckbox.setChecked(true);
                finish();
                startActivity(new Intent(this, MainActivity.class));
                return;
            }
            else{
                if (BsafeFinger) {
                    safeUsernamecheckbox.setChecked(false);
                    safeStayLoggedcheckbox.setChecked(true);
                    fingerprintLogin.startAuthentication(fingerprintManager, null);
                }
                else{
                    safeUsernamecheckbox.setChecked(false);
                    safeStayLoggedcheckbox.setChecked(true);
                }
            }
        }
        if(Bsafeusername && !BstayLogged){
            if (BsafeFinger) {
                editTextUsername.setText(sharedPreferencesUsername.getString("username", null));
                safeUsernamecheckbox.setChecked(true);
                safeStayLoggedcheckbox.setChecked(false);
                fingerprintLogin.startAuthentication(fingerprintManager, null);
            }
            else{
                editTextUsername.setText(sharedPreferencesUsername.getString("username", null));
                safeUsernamecheckbox.setChecked(true);
                safeStayLoggedcheckbox.setChecked(false);
            }
        }
        if(!BstayLogged && !Bsafeusername){
            if (BsafeFinger) {
                safeUsernamecheckbox.setChecked(false);
                safeStayLoggedcheckbox.setChecked(false);
                fingerprintLogin.startAuthentication(fingerprintManager, null);
            }
            else{
                safeUsernamecheckbox.setChecked(false);
                safeStayLoggedcheckbox.setChecked(false);
            }
        }
        progressDialog = new ProgressDialog(this);
        progressDialog.setMessage("Please wait...");
    }

    private void Safe_Checkbox_username(){
        final String Susername = editTextUsername.getText().toString().trim();

        if(safeUsernamecheckbox.isChecked()){
            Bsafeusername = safeUsernamecheckbox.isChecked();
            editorUsernname=sharedPreferencesUsername.edit();
            editorUsernname.putBoolean("Bsafeusername", true);
            editorUsernname.putString("username", Susername);
            editorUsernname.putBoolean("pref_check", Bsafeusername);
            editorUsernname.apply();
        }
        else{
            sharedPreferencesUsername.edit().clear().apply();
        }
    }

    private void Safe_Checkbox_StayLogged() {
        if(safeStayLoggedcheckbox.isChecked()){
            BstayLogged = safeStayLoggedcheckbox.isChecked();
            editorstayLogged = sharedPreferencesStayLogged.edit();
            editorstayLogged.putBoolean("BstayLogged", true);
            editorstayLogged.putBoolean("pref_check1", BstayLogged);
            editorstayLogged.apply();
        }
        else{
            sharedPreferencesStayLogged.edit().clear().apply();
        }
    }

    private void userLogin(){

        final String username = editTextUsername.getText().toString().trim();
        final String password = editTextPassword.getText().toString().trim();

        Safe_Checkbox_username();

        StringRequest stringRequest = new StringRequest(
                Request.Method.POST,
                Constants.URL_LOGIN,
                new Response.Listener<String>() {
                    @Override
                    public void onResponse(String response) {
                        progressDialog.dismiss();
                        try {
                            JSONObject obj = new JSONObject(response);
                            if(!obj.getBoolean("error")){
                                SharedPrefManager.getInstance(getApplicationContext())
                                        .userLogin(
                                                obj.getString("id"),
                                                obj.getString("username"),
                                                obj.getString("firstname"),
                                                obj.getString("sirname"),
                                                obj.getString("matrNr")
                                        );
                                if(BsafeFinger){
                                    fingerprintLogin.stopListening();
                                }
                                startActivity(new Intent(getApplicationContext(), MainActivity.class));
                                finish();
                            }else{
                                Toast.makeText(
                                        getApplicationContext(),
                                        obj.getString("message"),
                                        Toast.LENGTH_LONG
                                ).show();
                            }
                        } catch (JSONException e) {
                            e.printStackTrace();
                        }
                    }
                },
                new Response.ErrorListener() {
                    @Override
                    public void onErrorResponse(VolleyError error) {
                        progressDialog.dismiss();

                        Toast.makeText(
                                getApplicationContext(),
                                error.getMessage(),
                                Toast.LENGTH_LONG
                        ).show();
                    }
                }
        ){
            @Override
            protected Map<String, String> getParams(){
                Map<String, String> params = new HashMap<>();
                params.put("username", username);
                params.put("password", password);
                return params;
            }

        };
        VolleySingleton.getInstance(this).addToRequestQueue(stringRequest);
    }
}