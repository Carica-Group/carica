package semi_computing.carica;

import android.content.Context;
import android.support.annotation.NonNull;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.TextView;
import android.widget.Toast;

import java.util.ArrayList;

public class TransactionsAdapter extends RecyclerView.Adapter<TransactionsAdapter.ViewHolder>{

    //ArrayList vom Typ Transaction.
    private ArrayList<Transaction> transactions;

    private Context _context;

    TransactionsAdapter(Context context, ArrayList<Transaction> transactions){
        _context = context;
        this.transactions = transactions;
    }

    @NonNull
    @Override
    public ViewHolder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {

        View view = LayoutInflater.from(parent.getContext()).inflate(R.layout.list_item_transaction, parent, false);
        ViewHolder holder = new ViewHolder(view);
        return holder;
    }

    @Override
    public void onBindViewHolder(@NonNull ViewHolder holder, final int position) {

        //Adjust data to ViewHolder
        holder.betrag.setText(transactions.get(position).GetBetrag());
        holder.date.setText(transactions.get(position).GetDate());
        holder.paymentMethod.setImageResource(transactions.get(position).GetPaymentMethod());
        holder.paymentMethod.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                Toast.makeText(_context, transactions.get(position).GetPaymentMethodName(), Toast.LENGTH_SHORT).show();
            }
        });
    }

    @Override
    public int getItemCount() {
        return transactions.size();
    }

    public void AddListItems(ArrayList<Transaction> transactions){

        //Fügt neue Datensätze zur Liste hinzu und aktualisiert diese dann.
        this.transactions.addAll(transactions);
        this.notifyDataSetChanged();
    }

    class ViewHolder extends RecyclerView.ViewHolder{

        //Der ViewHolder ist eine Art Template für ein List-Item
        TextView betrag;
        TextView date;
        ImageView paymentMethod;

        ViewHolder(View itemView) {
            super(itemView);

            betrag = itemView.findViewById(R.id.list_item_betrag);
            date = itemView.findViewById(R.id.list_item_date);
            paymentMethod = itemView.findViewById(R.id.list_item_paymentMethod);

        }
    }
}
