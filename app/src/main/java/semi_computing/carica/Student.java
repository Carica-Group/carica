package semi_computing.carica;

import android.content.Context;
import android.content.Intent;
import android.util.Log;
import android.widget.Toast;

import com.android.volley.Request;
import com.android.volley.Response;
import com.android.volley.VolleyError;
import com.android.volley.toolbox.StringRequest;

import org.json.JSONException;
import org.json.JSONObject;

import java.util.HashMap;
import java.util.Map;


public class Student{

    public String username, firstname, sirname, id, matrNr, guthaben, card_status;

    private JSONObject JSON_Student;
    private Context _context;

    Student(Context context){
        _context = context;

        firstname = SharedPrefManager.getInstance(context).getfirstname();
        sirname = SharedPrefManager.getInstance(context).getsirname();
        matrNr = SharedPrefManager.getInstance(context).getmatrNr();
        username = SharedPrefManager.getInstance(context).getUsername();
        id = SharedPrefManager.getInstance(context).getid();
    }

    public void GetStudent(){

        StringRequest request = new StringRequest(Request.Method.POST, Constants.URL_STUDENT, new Response.Listener<String>() {
            @Override
            public void onResponse(String response){
                try {
                    JSON_Student = new JSONObject(response);
                    if(!JSON_Student.getBoolean("error")) {

                        guthaben = JSON_Student.getString("guthaben");
                        card_status = JSON_Student.getString("status");

                        Intent intent = new Intent();
                        intent.setAction("com.carica.broadcast.DB_TASK_COMPLETED");
                        _context.sendBroadcast(intent);
                    }
                    else{
                        Toast.makeText(
                                _context,
                                JSON_Student.getString("message"),
                                Toast.LENGTH_LONG
                        ).show();
                    }

                }catch(JSONException e){
                    e.printStackTrace();
                    Log.e("StudenJSON", "JSON error: " + e);
                }
            }
        }, new Response.ErrorListener() {
            @Override
            public void onErrorResponse(VolleyError error) {
                error.printStackTrace();
            }
        }){
            @Override
            protected Map<String, String> getParams(){
                Map<String, String> params = new HashMap<>();
                //The Post parameters
                params.put("id", String.valueOf(id));
                return params;
            }
        };

        VolleySingleton.getInstance(_context).addToRequestQueue(request);
    }



    public void UpdateCredit(final float money, final String paymentMethod){

        StringRequest request = new StringRequest(Request.Method.POST, Constants.URL_UPDATECREDIT, new Response.Listener<String>() {
            @Override
            public void onResponse(String response){
                try {
                    JSON_Student = new JSONObject(response);

                    if(!JSON_Student.getBoolean("error")){
                        guthaben = JSON_Student.getString("guthaben");

                        Intent intent = new Intent();
                        intent.setAction("com.carica.broadcast.DB_TASK_COMPLETED");
                        _context.sendBroadcast(intent);
                    }
                    else{
                        Toast.makeText(
                                _context,
                                JSON_Student.getString("message"),
                                Toast.LENGTH_LONG
                        ).show();
                    }

                }catch(JSONException e){
                    e.printStackTrace();
                    Log.e("StudenJSON", "JSON error: " + e);
                }
            }
        }, new Response.ErrorListener() {
            @Override
            public void onErrorResponse(VolleyError error) {
                error.printStackTrace();
            }
        }){
            @Override
            protected Map<String, String> getParams(){
                Map<String, String> params = new HashMap<>();
                //The Post parameters
                params.put("id", String.valueOf(id));
                params.put("money", String.valueOf(money));
                params.put("paymentMethod", paymentMethod);
                return params;
            }
        };

        VolleySingleton.getInstance(_context).addToRequestQueue(request);
    }

}

