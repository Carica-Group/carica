package semi_computing.carica;

import android.Manifest;
import android.app.KeyguardManager;
import android.content.Intent;
import android.content.SharedPreferences;
import android.content.pm.PackageManager;
import android.support.v4.hardware.fingerprint.FingerprintManagerCompat;
import android.os.Build;
import android.os.Bundle;
import android.support.annotation.RequiresApi;
import android.support.v4.content.ContextCompat;
import android.support.v7.app.AppCompatActivity;
import android.view.View;
import android.widget.ArrayAdapter;
import android.widget.Button;
import android.widget.CheckBox;
import android.widget.Spinner;
import android.widget.TextView;
import android.widget.Toast;

public class SettingsActivity extends AppCompatActivity implements View.OnClickListener {

    SharedPreferences sharedPreferencesFinger;
    SharedPreferences.Editor editorFinger;
    CheckBox safeFingercheckbox;
    Boolean BsafeFinger;
    FingerprintManagerCompat fingerprintManager;
    KeyguardManager keyguardManager;

    private Button logout, transactions;
    private TextView name, matrNr;
    private Student student;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_settings);

        //Logout
        logout = (Button) findViewById(R.id.logout);
        logout.setOnClickListener(this);

        transactions = (Button) findViewById(R.id.transactionButton);
        transactions.setOnClickListener(
                new View.OnClickListener() {
                    @Override
                    public void onClick(View v) {
                        Intent intent = new Intent(v.getContext(), TransactionsActivity.class);
                        startActivity(intent);
                    }
                }
        );

        //Get Student and set TextViews
        student = new Student(this);
        name = findViewById(R.id.Name);
        matrNr = findViewById(R.id.MatrNr);
        name.setText(String.format("%s %s", student.firstname, student.sirname));
        matrNr.setText(student.matrNr);

        //Fingerprint
        safeFingercheckbox = (CheckBox) findViewById(R.id.fingerprint);
        sharedPreferencesFinger = getSharedPreferences("Fingerref10", MODE_PRIVATE);
        BsafeFinger = sharedPreferencesFinger.getBoolean("BsafeFinger", false);

        keyguardManager = (KeyguardManager) getSystemService(KEYGUARD_SERVICE);
        fingerprintManager = FingerprintManagerCompat.from(getApplicationContext());//(FingerprintManagerCompat) getSystemService(FINGERPRINT_SERVICE);

        //Look if Fingerprintcheckbox is checked
        if (BsafeFinger) {
            safeFingercheckbox.setChecked(true);
        } else {
            safeFingercheckbox.setChecked(false);
        }

        //Safe Settings with Button
        safeFingercheckbox.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                CheckFingerPrem();
            }
        });
    }

    private void CheckFingerPrem() {
        //Check Fingerprintsettings
        if (safeFingercheckbox.isChecked()) {
            if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.M)
            {
                fingerprintManager = FingerprintManagerCompat.from(getApplicationContext());//(FingerprintManagerCompat) getSystemService(FINGERPRINT_SERVICE);

                if (!fingerprintManager.isHardwareDetected()&&BsafeFinger){
                    Toast.makeText(this, "Fingerprint authentication permission not enable", Toast.LENGTH_SHORT).show();
                    BsafeFinger = false;
                    safeFingercheckbox.setChecked(false);
                    editorFinger = sharedPreferencesFinger.edit();
                    editorFinger.putBoolean("BsafeFinger", false);
                    editorFinger.putBoolean("pref_check10", BsafeFinger);
                    editorFinger.commit();
                }
                else if((ContextCompat.checkSelfPermission(this, Manifest.permission.USE_FINGERPRINT)!= PackageManager.PERMISSION_GRANTED)){
                    Toast.makeText(this, "Permission not granted to use FingerPrint", Toast.LENGTH_SHORT).show();
                    BsafeFinger = false;
                    safeFingercheckbox.setChecked(false);
                    editorFinger = sharedPreferencesFinger.edit();
                    editorFinger.putBoolean("BsafeFinger", false);
                    editorFinger.putBoolean("pref_check10", BsafeFinger);
                    editorFinger.commit();
                }
                else if (!keyguardManager.isKeyguardSecure()){
                    Toast.makeText(this, "Lock screen security not enabled in Settings", Toast.LENGTH_SHORT).show();
                    BsafeFinger = false;
                    safeFingercheckbox.setChecked(false);
                    editorFinger = sharedPreferencesFinger.edit();
                    editorFinger.putBoolean("BsafeFinger", false);
                    editorFinger.putBoolean("pref_check10", BsafeFinger);
                    editorFinger.commit();
                }
                else if(!fingerprintManager.hasEnrolledFingerprints()){
                    Toast.makeText(this, "Register at least one fingerprint in Settings", Toast.LENGTH_SHORT).show();
                    BsafeFinger = false;
                    safeFingercheckbox.setChecked(false);
                    editorFinger = sharedPreferencesFinger.edit();
                    editorFinger.putBoolean("BsafeFinger", false);
                    editorFinger.putBoolean("pref_check10", BsafeFinger);
                    editorFinger.commit();
                }
                else{
                    BsafeFinger = safeFingercheckbox.isChecked();
                    safeFingercheckbox.setChecked(true);
                    editorFinger = sharedPreferencesFinger.edit();
                    editorFinger.putBoolean("BsafeFinger", true);
                    editorFinger.putBoolean("pref_check10", BsafeFinger);
                    editorFinger.commit();
                }
            }
            else{
                Toast.makeText(this, "Your systemversion does not support fingerprint", Toast.LENGTH_SHORT).show();
                BsafeFinger = false;
                safeFingercheckbox.setChecked(false);
                editorFinger = sharedPreferencesFinger.edit();
                editorFinger.putBoolean("BsafeFinger", false);
                editorFinger.putBoolean("pref_check10", BsafeFinger);
                editorFinger.commit();
            }
        } else {
            sharedPreferencesFinger.edit().clear().apply();
        }
    }

    @Override
    public void onClick(View view) {
        if (view == logout) {
            SharedPrefManager.getInstance(this).logout();
            finish();
            startActivity(new Intent(getApplicationContext(), LoginActivity.class));
        }
    }

}