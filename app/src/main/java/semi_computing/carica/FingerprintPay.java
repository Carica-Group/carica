package semi_computing.carica;

import android.app.Activity;
import android.content.Context;
import android.content.Intent;
import android.hardware.fingerprint.FingerprintManager;
import android.support.v4.hardware.fingerprint.FingerprintManagerCompat;
import android.support.v7.app.AppCompatActivity;
import android.widget.ImageView;
import android.widget.Toast;

public class FingerprintPay extends FingerprintSensor{

    private Context _context;
    private Activity _activity;

    FingerprintPay(Context context) {
        super(context);
    }

    public void onAuthenticationFailed() {
        Toast.makeText(context, "Authentifizierung fehlgeschlagen!", Toast.LENGTH_SHORT).show();

        Intent intent = new Intent();
        intent.setAction("com.carica.broadcast.FINGERPRINT_FAILED");
        _context.sendBroadcast(intent);
    }

    public void onAuthenticationSucceeded(FingerprintManagerCompat.AuthenticationResult result) {
        //Toast.makeText(context, "Erfolgreich!", Toast.LENGTH_SHORT).show();

        Intent intent = new Intent();
        intent.setAction("com.carica.broadcast.PW_CHECK_COMPLETED");
        _context.sendBroadcast(intent);
        _activity.finish();
    }

    public void SetContext(Context context){
        _context = context;
    }

    public void SetActivity(Activity activity){
        _activity = activity;
    }

}
