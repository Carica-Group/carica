package semi_computing.carica;

import android.content.Context;
import android.support.annotation.NonNull;
import android.util.Log;
import android.widget.Toast;

import com.adyen.core.PaymentRequest;
import com.adyen.core.interfaces.HttpResponseCallback;
import com.adyen.core.interfaces.PaymentDataCallback;
import com.adyen.core.interfaces.PaymentRequestListener;
import com.adyen.core.models.Payment;
import com.adyen.core.models.PaymentRequestResult;
import com.adyen.core.utils.AsyncHttpClient;

import org.json.JSONException;
import org.json.JSONObject;

import java.util.HashMap;
import java.util.Map;

public class PaymentController {
    private Context _context;
    private Student student;
    private float amountMoney = 0;
    private int adyenAmountValue = 0;
    private String method;

    //Constructor
    PaymentController(Context context, Student _student){_context = context; student = _student;}


    public void Pay(float money){

        if(money != 0) {
            amountMoney = (int) (money * 100);
            amountMoney /= 100;
            adyenAmountValue = (int) (money * 100);

            PaymentRequest paymentRequest = new PaymentRequest(_context, this.paymentRequestListener);
            paymentRequest.start();
        }
        else{
            Toast.makeText(_context, "Es wurde kein Betrag eingegeben!", Toast.LENGTH_SHORT).show();
        }
    }

    private PaymentRequestListener paymentRequestListener = new PaymentRequestListener() {
        @Override
        public void onPaymentDataRequested(@NonNull final PaymentRequest paymentRequest, @NonNull String s, @NonNull final PaymentDataCallback paymentDataCallback) {
            final Map<String, String> headers = new HashMap<>();
            headers.put("Content-Type", "application/json; charset=UTF-8");

            // Provide this data to identify your app against the server; implement your own protocol (e.g. OAuth 2.0) to access your own server.
            headers.put("X-API-Key", Constants.Checkout_DEMO_API_key); // Replace with your own Checkout Demo API key.

            final JSONObject jsonObject = new JSONObject();
            try {
                // You always need to get the token from the SDK, even when using your own server.
                jsonObject.put("token", s);
                // Below is dummy data in a format expected by the Adyen demo server. When implementing your own server, the data below becomes free format.
                // You can also decide to add it to the payment creation request while sending it from your own server.
                jsonObject.put("returnUrl", "app://checkout");
                jsonObject.put("countryCode", "DE");
                jsonObject.put("shopperLocale", "de_DE");
                jsonObject.put("merchantAccount", "TestaccountDE");
                final JSONObject amount = new JSONObject();
                amount.put("value", adyenAmountValue);
                amount.put("currency", "EUR");
                jsonObject.put("amount", amount);
                jsonObject.put("shopperReference", "example.merchant@adyen.com");
                jsonObject.put("channel", "android");
                jsonObject.put("reference", "test-payment");

                /*
                final JSONObject additionalData = new JSONObject();
                additionalData.put("androidpay.token", "");
                jsonObject.put("additionalData", additionalData);
                */
            } catch (final JSONException jsonException) {
                Log.e("Unexpected error", "Setup failed");
            }
            AsyncHttpClient.post(Constants.MERCHANT_SERVER_URL, headers, jsonObject.toString(), new HttpResponseCallback() { // Use https://checkoutshopper-test.adyen.com/checkoutshopper/demoserver/setup
                @Override
                public void onSuccess(final byte[] response) {
                    paymentDataCallback.completionWithPaymentData(response);
                }
                @Override
                public void onFailure(final Throwable e) {
                    paymentRequest.cancel();
                }
            });
        }


        private void verifyPayment(final String money)
        {
            final Map<String, String> headers= new HashMap<>();
            headers.put("Content-Type", "application/json; charset=UTF-8");
            headers.put("X-API-Key", Constants.Checkout_DEMO_API_key);

            final JSONObject jsonObject = new JSONObject();

            try{
                jsonObject.put("payload", money);
            }catch (final JSONException jsonException){
                Log.e("Unexpected error", jsonException.toString());
            }

            AsyncHttpClient.post(Constants.ADYEN_RESULT_URL, headers, jsonObject.toString(), new HttpResponseCallback() {
                @Override
                public void onSuccess(byte[] response) {

                    student.UpdateCredit(amountMoney, method);
                    Toast.makeText(_context, String.valueOf(amountMoney).replace(".", ",") + " € wurden aufgeladen.", Toast.LENGTH_LONG).show();

                }

                @Override
                public void onFailure(Throwable e) {
                    Toast.makeText(_context, "Zahlung leider fehlgeschlagen!", Toast.LENGTH_LONG).show();
                }
            });
        }

        @Override
        public void onPaymentResult(@NonNull PaymentRequest paymentRequest, @NonNull PaymentRequestResult paymentRequestResult) {
            if (paymentRequestResult.isProcessed() &&(
                    paymentRequestResult.getPayment().getPaymentStatus() == Payment.PaymentStatus.AUTHORISED
                            || paymentRequestResult.getPayment().getPaymentStatus() == Payment.PaymentStatus.RECEIVED)) {

                //Verify payment and update db
                verifyPayment(paymentRequestResult.getPayment().getPayload());
                method = paymentRequest.getPaymentMethod().getType();
                if(method == null){
                    method = "undefined";
                }

            } else {

                //Show error if transaction failed
                if(paymentRequestResult.getError() != null) {
                    /*
                    String error = paymentRequestResult.getError().toString();
                    Toast.makeText(_context, "Abbruch!", Toast.LENGTH_LONG).show();
                    */
                }
                else{
                    Toast.makeText(_context, "Fehler: Bitte überprüfe, ob deine Daten korrekt waren.", Toast.LENGTH_LONG).show();
                }
            }
        }

    };

}
