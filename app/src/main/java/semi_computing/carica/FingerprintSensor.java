package semi_computing.carica;

import android.content.Context;
import android.support.v4.hardware.fingerprint.FingerprintManagerCompat;
import android.support.v4.os.CancellationSignal;
import android.widget.Toast;

public class FingerprintSensor extends FingerprintManagerCompat.AuthenticationCallback {

    public Context context;
    CancellationSignal cancellationSignal = new CancellationSignal();
    public FingerprintSensor(Context context){
        this.context = context;
    }


    public void startAuthentication(FingerprintManagerCompat fingerprintManager, FingerprintManagerCompat.CryptoObject cryptoObject){
        fingerprintManager.authenticate(cryptoObject, 0, cancellationSignal,this, null);
    }


    public void stopListening() {
        if (cancellationSignal != null) {
            cancellationSignal.cancel();
            cancellationSignal = null;
        }
    }

    public void onAuthenticationHelp(int helpCode, CharSequence helpString) {
        Toast.makeText(context, "Fehler: " + helpString, Toast.LENGTH_SHORT).show();
    }

    public void onAuthenticationError(int errorCode, CharSequence errString) {
        /*if(cancellationSignal==null){
            return;
        }
        */
        //Toast.makeText(context, "There was an Authentication Error! " + errString, Toast.LENGTH_SHORT).show();
    }

    public void onAuthenticationFailed() {
        Toast.makeText(context, "Authentifizierung fehlgeschlagen!", Toast.LENGTH_SHORT).show();
    }
}
