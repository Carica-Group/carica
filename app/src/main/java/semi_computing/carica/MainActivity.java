package semi_computing.carica;

import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.Intent;
import android.content.IntentFilter;
import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;
import android.view.View;
import android.widget.Button;
import android.widget.ImageButton;
import android.widget.TextView;
import android.widget.Toast;

public class MainActivity extends AppCompatActivity {

    private ImageButton settings;
    private Student student;
    private boolean b_fuenf, b_zehn, b_load;

    TextView name, matrNr, guthaben, manuelAmount;

    private Button fuenf, zehn, load;

    PaymentController paymentController;


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);

        //Set status to logIn of true
        SharedPrefManager.getInstance(this).setTokenLogintrue();


        settings=findViewById(R.id.settings);
        settings.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                openSettingsActivity();
            }
        });

        //Set IntentFilter to declare from which service the bR_DatabaseTask receives messages
        IntentFilter filter = new IntentFilter("com.carica.broadcast.DB_TASK_COMPLETED");
        this.registerReceiver(bR_DatabaseTask, filter);

        IntentFilter pw_filter = new IntentFilter("com.carica.broadcast.PW_CHECK_COMPLETED");
        this.registerReceiver(bR_CheckPassword, pw_filter);

        //Reference to TextViewes
        name = findViewById(R.id.Name);
        matrNr = findViewById(R.id.MatrNr);
        guthaben = findViewById(R.id.Guthaben);
        manuelAmount = findViewById(R.id.Ladebetrag);

        //Reference to Buttons
        fuenf = findViewById(R.id.fuenf);
        zehn = findViewById(R.id.zehn);
        load = findViewById(R.id.aufladen);

        //Initialize student
        student = new Student(this);
        student.GetStudent();
        name.setText(String.format("%s %s", student.firstname, student.sirname));
        matrNr.setText(student.matrNr);


        paymentController = new PaymentController(this, student);

        fuenf.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v){

                b_fuenf = true;
                b_zehn = b_load = false;

                startActivity(new Intent (MainActivity.this, PopUp.class));
            }
        });

        zehn.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v){

                b_zehn = true;
                b_fuenf = b_load = false;

                startActivity(new Intent(MainActivity.this, PopUp.class));
            }
        });

        load.setOnClickListener(new View.OnClickListener(){
            @Override
            public void onClick(View v){

                b_load = true;
                b_fuenf = b_zehn = false;

                startActivity(new Intent(MainActivity.this, PopUp.class));
            }
        });

    }


    public void openSettingsActivity()
    {
        Intent intent = new Intent(this, SettingsActivity.class);
        startActivity(intent);
    }

    private void UpdateGuthabenView(){
        guthaben.setText(student.guthaben);
    }

    private BroadcastReceiver bR_CheckPassword = new BroadcastReceiver() {
        @Override
        public void onReceive(Context context, Intent intent) {

            if(b_fuenf){
                paymentController.Pay(5.00f);
            }
            else if(b_zehn){
                paymentController.Pay(10.00f);
            }
            else if(b_load) {
                String money = manuelAmount.getText().toString().replace(",", ".");

                try {
                    paymentController.Pay(Float.valueOf(money));
                } catch (NumberFormatException e) {
                    Toast.makeText(getApplicationContext(), "Falsche Eingabe!", Toast.LENGTH_SHORT).show();
                }
            }
            manuelAmount.setText("");
        }
    };

    private BroadcastReceiver bR_DatabaseTask = new BroadcastReceiver() {
        @Override
        public void onReceive(Context context, Intent intent) {
            UpdateGuthabenView();
        }
    };
}
