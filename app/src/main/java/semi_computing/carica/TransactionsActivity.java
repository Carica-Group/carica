package semi_computing.carica;

import android.os.Bundle;
import android.os.Handler;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.view.View;
import android.widget.Toast;

import com.android.volley.Request;
import com.android.volley.Response;
import com.android.volley.VolleyError;
import com.android.volley.toolbox.StringRequest;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.Map;

import static android.widget.AbsListView.OnScrollListener.SCROLL_STATE_IDLE;

public class TransactionsActivity extends AppCompatActivity {

    private TransactionsAdapter adapter;
    private ArrayList<Transaction> _transactions = new ArrayList<>();
    private LinearLayoutManager manager;
    private JSONArray transaktion;

    //ProgressBar (kleiner Spinner), der beim laden von neuen Datensätzen angezeigt wird.
    private View loader;

    //i for counting the transactions in history
    private int i;
    private int _offset = 0;
    private final int LIMIT = 10;

    @Override
    protected void onCreate(Bundle savedInstanceState){
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_transactions);

        //Initialization
        manager = new LinearLayoutManager(this);
        loader = findViewById(R.id.loader);

        initRecyclerView();
    }

    private void initRecyclerView(){

        //Initialisiert die RecyclerView
        RecyclerView recyclerView = findViewById(R.id.recyclerView);
        adapter = new TransactionsAdapter(this, _transactions);
        recyclerView.setAdapter(adapter);
        recyclerView.setLayoutManager(manager);

        LoadData();

        //onScrollListener um zu schaun, ob das Ende der Liste erreicht wurde und ggf. neue Daten zu laden.
        recyclerView.addOnScrollListener(new RecyclerView.OnScrollListener() {
            @Override
            public void onScrollStateChanged(RecyclerView recyclerView, int newState) {
                super.onScrollStateChanged(recyclerView, newState);

                if (!recyclerView.canScrollVertically(View.SCROLL_AXIS_VERTICAL) && newState == SCROLL_STATE_IDLE)
                {
                    //Das Ende der Liste wurde erreicht
                    LoadData();
                }
            }
        });
    }

    //LoadData lädt neue Datensätze.
    //Die postDelayed() Methode vom Handler ist nur dazu da, um die Ladezeit der Funktion zu verzögern,
    //damit man den Spinner sieht.

    private void LoadData()
    {
        i = 0;

        StringRequest request = new StringRequest(Request.Method.POST, Constants.TRANSACTION_URL, new Response.Listener<String>()
        {
            @Override
            public void onResponse(String response)
            {
                try {
                    JSONObject jsonObject = new JSONObject(response);
                    if(!jsonObject.getBoolean("error"))
                    {
                        final ArrayList<Transaction> transactions = new ArrayList<>();
                        //got a Transaction, set offset to next position
                        transaktion = jsonObject.toJSONArray(jsonObject.names()); //Convert into JSONArray 0 is error and 1 is the Transaktionhistory
                        JSONArray transactionsArray = transaktion.getJSONArray(1);  //get only the Transaktions as a Array
                        //load max. 10 Transaktions
                        while(i<LIMIT)
                        {
                            if(transactionsArray.isNull(i))     //check if a Transaktionelement exists
                            {
                                break;
                            }
                            else
                            {
                                Transaction first;
                                first = new Transaction(transactionsArray.getJSONObject(i).getString("betrag"), transactionsArray.getJSONObject(i).getString("payment_method") ,transactionsArray.getJSONObject(i).getString("timestamp"));
                                //Ein Arrayelement wird zerlegt und als Transaction in die History geschrieben
                                transactions.add(first);

                                _offset++;
                            }
                            i++;
                        }

                        loader.setVisibility(View.VISIBLE);

                        new Handler().postDelayed(new Runnable() {
                            @Override
                            public void run() {

                                adapter.AddListItems(transactions);
                                loader.setVisibility(View.GONE);
                            }
                        }, 1000);

                    }
                    else{
                        String error = jsonObject.getString("message");
                        Toast.makeText(getApplicationContext(), error, Toast.LENGTH_LONG).show();
                    }
                }catch(JSONException e){
                    Toast.makeText(getApplicationContext(), e.toString(), Toast.LENGTH_LONG).show();
                }
            }
        }, new Response.ErrorListener()
        {
            @Override
            public void onErrorResponse(VolleyError error)
            {
                Toast.makeText(getApplicationContext(), error.toString(), Toast.LENGTH_LONG).show();
            }
        }) {
            @Override
            protected Map<String, String> getParams() {
                Map<String, String> params = new HashMap<>();
                //The Post parameters
                params.put("id", (SharedPrefManager.getInstance(getApplicationContext()).getid()));
                params.put("offset", String.valueOf(_offset));
                params.put("limit", String.valueOf(LIMIT));

                return params;
            }
        };

        VolleySingleton.getInstance(this).addToRequestQueue(request);
    }
}
