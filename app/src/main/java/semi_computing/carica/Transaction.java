package semi_computing.carica;

import java.text.NumberFormat;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.Currency;
import java.util.Date;
import java.util.Locale;

/*
    Gibt die Struktur einer Transaktion wieder.
    Auf die Membervariablen kann per Getters zugegriffen werden.
    Der Konstruktor weisst ihnen die Werte zu
 */
public class Transaction {

    private String _betrag;
    private String _date;
    private String _paymentMethod;

    Transaction(String betrag,String paymentMethod, String dateStr){

        //Convert number-format
        Currency euros = Currency.getInstance("EUR");
       NumberFormat numberFormat = NumberFormat.getCurrencyInstance(Locale.GERMANY);
       numberFormat.setCurrency(euros);

        _betrag = numberFormat.format(Float.valueOf(betrag));

        _paymentMethod = paymentMethod;

        //Convert time-format
        SimpleDateFormat sdfSource = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss", Locale.GERMANY);
        Date date = null;
        try {
            date = sdfSource.parse(dateStr);
        } catch (ParseException e) {
            e.printStackTrace();
        }
        SimpleDateFormat sdfDestination = new SimpleDateFormat("dd.MM.yy HH:mm", Locale.getDefault());

        _date = sdfDestination.format(date);
    }

    public String GetBetrag(){
        return _betrag;
    }

    public String GetDate(){
        return _date;
    }

    public int GetPaymentMethod(){

        switch (_paymentMethod){
            case "mc":                      //Mastercard
                return R.drawable.mc;
            case "card":                    //Kreditkarte
                return R.drawable.card;
            case "directEbanking":          //Sofort
                return R.drawable.direct_ebanking;
            case "sepadirectdebit":         //SEPA
                return R.drawable.sepadirectdebit;
            case "giropay":                 //GiroPay
                return R.drawable.giropay;
            default:
                return 0;

        }
    }

    public String GetPaymentMethodName(){

        switch (_paymentMethod){
            case "mc":                      //Mastercard
                return "Mastercard";
            case "card":                    //Kreditkarte
                return "Kreditkarte";
            case "directEbanking":          //Sofort.
                return "Sofort.";
            case "sepadirectdebit":         //SEPA
                return "SEPA Direct Depit";
            case "giropay":                 //GiroPay
                return "GiroPay";
            default:
                return "undefined";
        }
    }
}
