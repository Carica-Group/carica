package semi_computing.carica;

import android.content.Context;
import android.content.Intent;
import android.support.v4.hardware.fingerprint.FingerprintManagerCompat;

public class FingerprintLogin extends FingerprintSensor {
    FingerprintLogin(Context context) {
        super(context);
    }

    public void onAuthenticationSucceeded(FingerprintManagerCompat.AuthenticationResult result) {
        //Toast.makeText(context, "Success!", Toast.LENGTH_SHORT).show();
        context.startActivity(new Intent(context,MainActivity.class));
    }
}
